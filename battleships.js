const shipFactory = (length,sunk) => {
  const positionsHit=[];
  this.sunk = sunk || false

  const hit = position =>{
    if (position <= length && positionsHit.includes(position) === false){
      positionsHit.push(position);
    }
  }

  const isSunk = () =>{
    if (positionsHit.length === length){
      this.sunk =true ;
    }
  }

  return {sunk,hit,length,positionsHit,isSunk};

};

module.exports = shipFactory;
