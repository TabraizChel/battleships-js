const shipFactory = require('./battleships');

describe('battleship', ()=>{
  test("length", function(){
    const battleship = shipFactory(7);
    expect(battleship.length).toEqual(7);
  });

  test("positions hit", function(){
    const battleship = shipFactory(2);
    battleship.hit(1);
    battleship.hit(2);
    expect(battleship.positionsHit).toEqual([1,2]);
  });

  test("sunk", function(){
    const battleship = shipFactory(2,false);
    battleship.hit(1);
    battleship.hit(2);
    battleship.hit(0);
    battleship.isSunk()
    expect(battleship.sunk).toEqual(true);
  });
});
